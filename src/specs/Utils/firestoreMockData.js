import moment from 'moment'

export const ordered = {
    groups: [
        {
            id: "ARoQm7bLwA9GSijKPckp",
            title: "Fake Group Title 1!",
            createdAt: moment(),
            description: "test description 1",
            leaderFirstName: "John",
            leaderId: "BzYJKMyfKkN8QV2JwirK3WFH94u1",
            leaderLastName: "Doe",
            members: ["BzYJKMyfKkN8QV2JwirK3WFH94u1", "BzDJKMyfKkN8QV2JwirK3WFH94u1"]
        },
        {
            id: "ARoQm7bLwA9GSijKPckp",
            title: "Fake Group Title 2!",
            createdAt: moment(),
            description: "test description 2",
            leaderFirstName: "Jane",
            leaderId: "BzYJKMyfKkN8QV2JwirK3WFH94u1",
            leaderLastName: "Doe",
            members: ["CzYJKMyfKkN8QV2JwirK3WFH94u1", "CzDJKMyfKkN8QV2JwirK3WFH94u1"]
        }
    ],
    notifications: [
        {
            content: "New User Joined 1",
            id: "82ZeWfu84oqZYn4O0JOA",
            time: moment(),
            user: "Jane Doe"
        },
        {
            content: "Created a Group",
            id: "82ZeWfu84oqZYn4O0JOA",
            time: moment(),
            user: "John Doe"
        }
     ],
     groupnotifications: [
        {
            content: "New User Joined 1",
            id: "72ZeWfu84oqZYn4O0JOA",
            time: moment(),
            user: "Jane Doe"
        },
        {
            content: "Created a Group",
            id: "62ZeWfu84oqZYn4O0JOA",
            time: moment(),
            user: "John Doe"
        }
     ],
    discussions: [
        {
            authorFirstName: "John",
            authorId: "BzDJKMyfKkN8QV2JwirK3WFH94u1",
            authorLastName: "Doe",
            content: "This is a test discussion 1",
            createdAt: moment(),
            groupId: "DRoQm7bLwA9GSijKPckp",
            id: "bSjl5CeSUoNOzNrkSxbH",
            title: "Discussion Title 1"
        },
        {
            authorFirstName: "Jane",
            authorId: "XzYJKMyfKkN8QV2JwirK3WFH94u1",
            authorLastName: "Doe",
            content: "This is a test discussion 2",
            createdAt: moment(),
            groupId: "BRoQm7bLwA9GSijKPckp",
            id: "aSjl5CeSUoNOzNrkSxbH",
            title: "Discussion Title 2"
        }
    ],
    comments: [
        {
            commenterName: "John Doe",
            text: "fake comment 1",
            createdAt: moment(),
        },
        {
            commenterName: "Jane Doe",
            text: "fake comment 2",
            createdAt: moment(),
        }
    ]
}

export const data = {
    discussions: {
        bSjl5CeSUoNOzNrkSxbH: {
            authorFirstName: "John",
            authorId: "BzDJKMyfKkN8QV2JwirK3WFH94u1",
            authorLastName: "Doe",
            content: "This is a test discussion 1",
            createdAt: moment(),
            groupId: "DRoQm7bLwA9GSijKPckp",
            title: "Discussion Title 1"
        },
        aSjl5CeSUoNOzNrkSxbH: {
            authorFirstName: "Jane",
            authorId: "XzYJKMyfKkN8QV2JwirK3WFH94u1",
            authorLastName: "Doe",
            content: "This is a test discussion 2",
            createdAt: moment(),
            groupId: "BRoQm7bLwA9GSijKPckp",
            title: "Discussion Title 2"
        }
    },
    groups: {
        cSjl5CeSUoNOzNrkSxbH: {
            leaderFirstName: "John",
            leaderId: "BzDJKMyfKkN8QV2JwirK3WFH94u1",
            authorLastName: "Doe",
            description: "This is a test group 1",
            createdAt: moment(),
            groupId: "DRoQm7bLwA9GSijKPckp",
            title: "Group Title 1",
            members: ["BzDJKMyfKkN8QV2JwirK3WFH94u1"]
        },
        aSjl5CeSUoNOzNrkSxbH: {
            leaderLastName: "Jane",
            leaderId: "XzYJKMyfKkN8QV2JwirK3WFH94u1",
            authorLastName: "Doe",
            description: "This is a test group 2",
            createdAt: moment(),
            groupId: "BRoQm7bLwA9GSijKPckp",
            title: "Group Title 2",
            members: ["CzYJKMyfKkN8QV2JwirK3WFH94u1", "CzDJKMyfKkN8QV2JwirK3WFH94u1"]
        }
    },
    users: {
        BzDJKMyfKkN8QV2JwirK3WFH94u1: {
            firstName: "John",
            initials: "JD",
            lastName: "Doe"
        }
    }
}