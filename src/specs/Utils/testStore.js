import { applyMiddleware, createStore } from 'redux'
import rootReducer from '../../store/reducers/rootReducer'
import thunk from 'redux-thunk'

export const testStore = (initState) => {
    const createStoreWithMiddleware = applyMiddleware(thunk)(createStore)
    return createStoreWithMiddleware(rootReducer, initState)
}