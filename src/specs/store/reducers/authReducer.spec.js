import authReducer from '../../../store/reducers/authReducer';

describe('authReducer', () => {
    
    it('Should return default state', () => {
        const defaultState = authReducer(undefined, {})
        expect(defaultState.authError).toEqual(null)
    })

})