import groupReducer from '../../../store/reducers/groupReducer';

describe('groupReducer', () => {
    
    it('Should return default state', () => {
        const defaultState = groupReducer(undefined, {})
        expect(defaultState.firebaseResponse).toEqual({
            error: null, 
            pending: false,
            fulfilled: false
        })
    })

})