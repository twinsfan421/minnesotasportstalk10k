import commentReducer from '../../../store/reducers/commentReducer';

describe('commentReducer', () => {
    
    it('Should return default state', () => {
        const defaultState = commentReducer(undefined, {})
        expect(defaultState.error).toEqual(null)
    })

})
