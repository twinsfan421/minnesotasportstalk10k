import discussionReducer from '../../../store/reducers/discussionReducer';

describe('discussionReducer', () => {
    
    it('Should return default state', () => {
        const defaultState = discussionReducer(undefined, {})
        expect(defaultState.firebaseResponse).toEqual({
            error: null, 
            pending: false,
            fulfilled: false
        })
    })

})