import React from 'react'
import { shallow } from 'enzyme'
import moment from 'moment'
import DiscussionForm from '../../../components/forms/DiscussionForm';
import DiscussionEdit from '../../../components/discussions/DiscussionEdit'

const setUp = (props={}) => {
    const component = shallow(<DiscussionForm {...props} />)
    return component
}

describe("DiscussionFormComponent", () => {

    let wrapper
    let component
    beforeEach(() => {
        const props = {
            discussion: { id: "Crvgzy4sTvA0qPqtKfWD",
            title: "Discussion title",
            authorFirstName: "John",
            authorLastName: "Doe",
            createdAt: moment(), 
            content: "fake comment"},
            firebaseResponse: {error: null, pending: false, fulfilled: false}
        };
        wrapper = setUp(props);
        component = wrapper.find(".discussion-form")
    })
    
    it('should rend without errors', () => {
        expect(component.length).toBe(1)
    })

    it('form title should have default value', () => {
        expect(wrapper.find("#title").prop("defaultValue")).toEqual('Discussion title');
    })
    
    it('form content should have default value', () => {
        expect(wrapper.find("#content").prop("defaultValue")).toEqual('fake comment');
    })
});
