import React from 'react'
import { shallow } from 'enzyme'
import moment from 'moment'
import GroupForm from '../../../components/forms/GroupForm';

const setUp = (props={}) => {
    const component = shallow(<GroupForm {...props} />)
    return component
}

describe("GroupForm Component", () => {

    let wrapper
    let component
    beforeEach(() => {
        const props = {
            group:{
            id: "Drvgzy4sTvA0qPqtKfWD",
            title: "Group title",
            leaderFirstName: "John",
            leaderLastName: "Doe",
            createdAt: moment(), 
            description: "fake group"},
            firebaseResponse: {error: null, pending: false, fulfilled: false}
        };
        wrapper = setUp(props);
        component = wrapper.find(".white")
    })
    

    it('should rend without errors', () => {
        expect(component.length).toBe(1)
    })

    it('form title should have default value', () => {
        expect(wrapper.find("#title").prop("defaultValue")).toEqual('Group title');
    })

    it('form content should have default value', () => {
        expect(wrapper.find("#description").prop("defaultValue")).toEqual('fake group');
    })
});