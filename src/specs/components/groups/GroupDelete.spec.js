import React from 'react'
import { shallow } from 'enzyme'
import { testStore } from '../../Utils/testStore';
import firebaseMockData from '../../Utils/firebaseMockData'
import { data } from '../../Utils/firestoreMockData'
import GroupDelete from '../../../components/groups/GroupDelete';
import { Redirect } from 'react-router-dom'

const setUp = (initState, props) => {
    const store = testStore(initState)
    const wrapper = shallow(<GroupDelete store={store} {...props} />).dive({context: {store: store}}).dive()
    return wrapper
}

describe("GroupDelete Component", () => {

    describe("group leaderId matches user", () => {

        let wrapper
        beforeEach(() => {
            const initState = {
                    firebase: firebaseMockData,
                    firestore: { data: data }
            }
            const props = { match: { params: {id: "cSjl5CeSUoNOzNrkSxbH"}}}
            wrapper = setUp(initState, props)
        })

        it('should render without errors', () => {
            expect(wrapper.find(".group-delete").length).toBe(1)
        })

        it('should have h3', () => {
            expect(wrapper.find("h3").text()).toEqual('Are you sure you want to delete this Group?');
        })

        it('should render group title', () => {
            expect(wrapper.find(".card-title").text()).toEqual('Group Title 1');
        })
    })

    describe("group leaderId does not match user", () => {

        let wrapper
        beforeEach(() => {
            const initState = {
                    firebase: firebaseMockData,
                    firestore: { data: data }
            }
            const props = { match: { params: {id: "aSjl5CeSUoNOzNrkSxbH"}}}
            wrapper = setUp(initState, props)
        })

        it('should redirect the user to home', () => {
            expect(wrapper.containsMatchingElement(<Redirect to="/" />)).toBe(true)
        })
    })
});