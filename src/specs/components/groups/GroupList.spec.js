import React from 'react'
import { shallow } from 'enzyme'
import moment from 'moment'
import GroupList from '../../../components/groups/GroupList';
import { Link } from 'react-router-dom'
import GroupSummary from '../../../components/groups/GroupSummary'

const setUp = (props={}) => {
    const component = shallow(<GroupList {...props} />)
    return component
}

describe("DiscussionSummary Component", () => {

    let wrapper
    let component
    beforeEach(() => {
        const props = {
            groups: [{ 
            id: "Drvgzy4sTvA0qPqtKfWD",
            title: "Group title",
            leaderFirstName: "John",
            leaderLastName: "Doe",
            createdAt: moment(), 
            content: "fake comment"
            }]
        };
        wrapper = setUp(props);
        component = wrapper.find(".group-list")
    })
    

    it('should rend without errors', () => {
        expect(component.length).toBe(1)
    })

    it('should map GroupSummary Link', () => {
        expect(component.containsMatchingElement(<Link to="/groups/Drvgzy4sTvA0qPqtKfWD"><GroupSummary /></Link>)).toBe(true)
    })
});