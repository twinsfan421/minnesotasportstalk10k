import React from 'react'
import { shallow } from 'enzyme'
import { testStore } from '../../Utils/testStore';
import firebaseMockData from '../../Utils/firebaseMockData'
import { data } from '../../Utils/firestoreMockData'
import GroupEdit from '../../../components/groups/GroupEdit';
import { Redirect } from 'react-router-dom'

const setUp = (initState, props) => {
    const store = testStore(initState)
    const wrapper = shallow(<GroupEdit store={store} {...props} />).dive({context: {store: store}}).dive()
    return wrapper
}

describe("GroupEdit Component", () => {

    describe("group leaderId matches user", () => {

        let wrapper
        beforeEach(() => {
            const initState = {
                    firebase: firebaseMockData,
                    firestore: { data: data }
            }
            const props = { match: { params: {id: "cSjl5CeSUoNOzNrkSxbH"}}}
            wrapper = setUp(initState, props)
        })

        it('should render without errors', () => {
            expect(wrapper.find(".group-edit").length).toBe(1)
        })

        it('should have h3', () => {
            expect(wrapper.find("h3").text()).toEqual('Edit Group')
        })
    })

    describe("discussion leaderId does not match user", () => {

        let wrapper
        beforeEach(() => {
            const initState = {
                    firebase: firebaseMockData,
                    firestore: { data: data }
            }
            const props = { match: { params: {id: "aSjl5CeSUoNOzNrkSxbH"}}}
            wrapper = setUp(initState, props)
        })

        it('should redirect the user to home', () => {
            expect(wrapper.containsMatchingElement(<Redirect to="/" />)).toBe(true)
        })
    })
});