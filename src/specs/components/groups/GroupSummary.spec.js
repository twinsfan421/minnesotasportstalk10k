import React from 'react'
import { shallow } from 'enzyme'
import moment from 'moment'
import GroupSummary from '../../../components/groups/GroupSummary';

const setUp = (props={}) => {
    const component = shallow(<GroupSummary {...props} />)
    return component
}

describe("GroupSummary Component", () => {

    let wrapper
    let component
    beforeEach(() => {
        const props = {
            group:{
            id: "Drvgzy4sTvA0qPqtKfWD",
            title: "Group title",
            leaderFirstName: "John",
            leaderLastName: "Doe",
            createdAt: moment(), 
            description: "fake group"}
        };
        wrapper = setUp(props);
        component = wrapper.find(".group-summary")
    })
    

    it('should rend without errors', () => {
        expect(component.length).toBe(1)
    })

    it('should display Group title', () => {
        expect(wrapper.find(".card-title").text()).toEqual('Group title');
    })
});