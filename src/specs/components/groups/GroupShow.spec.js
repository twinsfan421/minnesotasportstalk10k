import React from 'react'
import { shallow } from 'enzyme'
import { testStore } from '../../Utils/testStore';
import firebaseMockData from '../../Utils/firebaseMockData'
import { data, ordered } from '../../Utils/firestoreMockData'
import GroupShow from '../../../components/groups/GroupShow';

const setUp = (initState, props) => {
    const store = testStore(initState)
    const wrapper = shallow(<GroupShow store={store} {...props} />).dive({context: {store: store}}).dive()
    return wrapper
}

describe("GroupShow Component", () => {

    let wrapper
    beforeEach(() => {
        const initState = {
                firebase: firebaseMockData,
                firestore: { data: data, ordered: ordered}
        }
        const props = { match: { params: {id: "cSjl5CeSUoNOzNrkSxbH"}}}
        wrapper = setUp(initState, props)
    })

    it('should render without errors', () => {
        expect(wrapper.find(".group-show").length).toBe(1)
    })

    it('should have a h3 group title', () => {
        expect(wrapper.find("h3").text()).toEqual('Group Title 1');
    })

    it('should have a h5 group description', () => {
        expect(wrapper.find("h5").text()).toEqual('This is a test group 1');
    })
});