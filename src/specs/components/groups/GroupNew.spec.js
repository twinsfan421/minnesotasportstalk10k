import React from 'react'
import { shallow } from 'enzyme'
import { testStore } from '../../Utils/testStore';
import firebaseMockData from '../../Utils/firebaseMockData'
import GroupNew from '../../../components/groups/GroupNew';

const setUp = (initState) => {
    const store = testStore(initState)
    const wrapper = shallow(<GroupNew store={store} />).dive()
    return wrapper
}

describe("CreateGroup Component", () => {

    let wrapper
    beforeEach(() => {
        const initState = {
                firebase: firebaseMockData
        }
        wrapper = setUp(initState)
    })

    it('should render without errors', () => {
        const component = wrapper.find(".create-group")
        expect(component.length).toBe(1)
    })

    it('should have h3', () => {
        expect(wrapper.find("h3").text()).toEqual('New Group')
    })

});