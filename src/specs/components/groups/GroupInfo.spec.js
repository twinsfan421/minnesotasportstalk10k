import React from 'react'
import { shallow } from 'enzyme'
import moment from 'moment'
import GroupInfo from '../../../components/groups/GroupInfo';

const setUp = (props={}) => {
    const component = shallow(<GroupInfo {...props} />)
    return component
}

describe("GroupInfo Component", () => {

    let wrapper
    let component
    beforeEach(() => {
        const props = {
            group: { 
            id: "Drvgzy4sTvA0qPqtKfWD",
            title: "Group title",
            leaderFirstName: "John",
            leaderLastName: "Doe",
            createdAt: moment(),
            updatedAt: null,
            content: "fake comment",
            members: ["Drvgzy4sTvA0qPqtKfWD"]
            }
        };
        wrapper = setUp(props);
        component = wrapper.find(".group-info")
    })
    

    it('should rend without errors', () => {
        expect(component.length).toBe(1)
    })

    it('should render group title', () => {
        expect(wrapper.find("span").text()).toEqual('Group Creator: John Doe');
    })

    it('should have members count', () => {
        expect(component.containsMatchingElement(<p className="white-text">{"Members Count: 1"}</p>)).toBe(true)
    })
});