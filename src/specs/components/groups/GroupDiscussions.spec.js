import React from 'react'
import { shallow } from 'enzyme'
import { testStore } from '../../Utils/testStore';
import firebaseMockData from '../../Utils/firebaseMockData'
import { ordered, data } from '../../Utils/firestoreMockData'
import GroupDiscussions from '../../../components/groups/GroupDiscussions';

const setUp = (initState, props) => {
    const store = testStore(initState)
    const wrapper = shallow(<GroupDiscussions store={store} {...props} />).dive({context: {store: store}}).dive()
    return wrapper
}

describe("GroupDiscussions Component", () => {

    let wrapper
    beforeEach(() => {
        const initState = {
                firebase: firebaseMockData,
                firestore: { 
                    ordered: ordered,
                    data: data
                }
        }
        const props = { match: { params: {id: "cSjl5CeSUoNOzNrkSxbH"}}}
        wrapper = setUp(initState, props)
    })

    it('should render without errors', () => {
        const component = wrapper.find(".group-discussions")
        expect(component.length).toBe(1)
    })

    it('have 2 child links', () => {
        expect(wrapper.find(".col").children().length).toBe(1)
    })
});