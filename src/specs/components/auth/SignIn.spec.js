import React from 'react'
import { shallow } from 'enzyme'
import { testStore } from '../../Utils/testStore';
import SignIn from '../../../components/auth/SignIn';

const setUp = (initState) => {
    const store = testStore(initState)
    const wrapper = shallow(<SignIn store={store} />).dive()
    return wrapper
}

describe("SignIn Component", () => {

    describe("with no errors", () => {
        let wrapper
        beforeEach(() => {
            const initState = {
                auth: {authError: null},
                firebase: {auth: {uid: null}}
            }
            wrapper = setUp(initState)
        })

        it('should render without errors', () => {
            const component = wrapper.find(".sign-in")
            expect(component.length).toBe(1)
        })

        it('should not display error <p>', () => {
            expect(wrapper.find(".red-text").length).toBe(0)
        })
    });

    describe("with errors", () => {
        let wrapper
        beforeEach(() => {
            const initState = {
                    auth: {authError: "some error"},
                    firebase: {auth: {uid: null}}
            }
            wrapper = setUp(initState)
        })

        it('should render without errors', () => {
            const component = wrapper.find(".sign-in")
            expect(component.length).toBe(1)
        })

        it('should render error text', () => {
            expect(wrapper.containsMatchingElement(<p className="red-text center">some error</p>)).toBe(true)
        })
    });
});