import React from 'react'
import { shallow } from 'enzyme'
import Notifications from '../../../components/dashboard/Notifications';
import moment from 'moment'

const setUp = (props={}) => {
    const component = shallow(<Notifications {...props} />)
    return component
}

describe("Notifications Component", () => {

    describe("Has valid props", () => {

        let wrapper
        let component
        beforeEach(() => {
            const props = {
                notifications: [{ id: "Crvgzy4sTvA0qPqtKfWD", user:"John Doe", time: moment(), content: "fake notification"}]
            };
            wrapper = setUp(props);
            component = wrapper.find(".notifications")
        })
        
    
        it('should rend without errors', () => {
            expect(component.length).toBe(1)
        })

        it('should map notifications', () => {
            expect(component.find('ul').children().length).toBe(1)
        })
    });

    describe("Have No props", () => {

        let wrapper
        let component
        beforeEach(() => {
            wrapper = setUp();
            component = wrapper.find(".notifications")
        })

        it('should rend without errors', () => {
            expect(component.length).toBe(1)
        })

        it('should not map notifcations', () => {
            expect(component.find('ul').children().length).toBe(0)
        })
    });
});