import React from 'react'
import { shallow } from 'enzyme'
import { testStore } from '../../Utils/testStore';
import firebaseMockData from '../../Utils/firebaseMockData'
import { ordered } from '../../Utils/firestoreMockData'
import Dashboard from '../../../components/dashboard/Dashboard';

const setUp = (initState) => {
    const store = testStore(initState)
    const wrapper = shallow(<Dashboard store={store} />).dive({context: {store: store}}).dive()
    return wrapper
}

describe("Dashboard Component", () => {

    let wrapper
    beforeEach(() => {
        const initState = {
                firebase: firebaseMockData,
                firestore: { ordered: ordered }
        }
        wrapper = setUp(initState)
    })

    it('should render without errors', () => {
        const component = wrapper.find(".dashboard")
        expect(component.length).toBe(1)
    })
});