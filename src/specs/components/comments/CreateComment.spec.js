import React from 'react'
import { shallow } from 'enzyme'
import { testStore } from '../../Utils/testStore';
import CreateComment from '../../../components/comments/CreateComment';

const setUp = (initState) => {
    const store = testStore(initState)
    const wrapper = shallow(<CreateComment store={store} />).dive()
    return wrapper
}

describe("CreateComment Component", () => {

    let wrapper
    beforeEach(() => {
        wrapper = setUp({})
    })

    it('should render without errors', () => {
        const component = wrapper.find(".create-comment")
        expect(component.length).toBe(1)
    })
});