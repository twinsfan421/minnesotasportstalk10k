import React from 'react'
import { shallow } from 'enzyme'
import moment from 'moment'
import DiscussionSummary from '../../../components/discussions/DiscussionSummary';

const setUp = (props={}) => {
    const component = shallow(<DiscussionSummary {...props} />)
    return component
}

describe("DiscussionSummary Component", () => {

    let wrapper
    let component
    beforeEach(() => {
        const props = {
            discussion: { id: "Crvgzy4sTvA0qPqtKfWD",
            title: "Discussion title",
            authorFirstName: "John",
            authorLastName: "Doe",
            createdAt: moment(), 
            content: "fake comment"}
        };
        wrapper = setUp(props);
        component = wrapper.find(".discussion-summary")
    })
    

    it('should rend without errors', () => {
        expect(component.length).toBe(1)
    })

    it('should display title', () => {
        expect(wrapper.find(".card-title").text()).toEqual('Discussion title');
    })
});