import React from 'react'
import { shallow } from 'enzyme'
import { testStore } from '../../Utils/testStore';
import firebaseMockData from '../../Utils/firebaseMockData'
import { data } from '../../Utils/firestoreMockData'
import DiscussionShow from '../../../components/discussions/DiscussionShow';

const setUp = (initState, props) => {
    const store = testStore(initState)
    const wrapper = shallow(<DiscussionShow store={store} {...props} />).dive({context: {store: store}}).dive()
    return wrapper
}

describe("DiscussionShow Component", () => {

    let wrapper
    beforeEach(() => {
        const initState = {
                firebase: firebaseMockData,
                firestore: { data: data }
        }
        const props = { match: { params: {id: "bSjl5CeSUoNOzNrkSxbH"}}}
        wrapper = setUp(initState, props)
    })

    it('should render without errors', () => {
        expect(wrapper.find(".discussion-details").length).toBe(1)
    })
    
});