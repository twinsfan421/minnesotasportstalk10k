import React from 'react'
import { shallow } from 'enzyme'
import moment from 'moment'
import DiscussionInfo from '../../../components/discussions/DiscussionInfo';

const setUp = (props={}) => {
    const component = shallow(<DiscussionInfo {...props} />)
    return component
}

describe("DiscussionInfo Component", () => {

    let wrapper
    let component
    beforeEach(() => {
        const props = {
            discussion: { id: "Crvgzy4sTvA0qPqtKfWD",
            title: "Discussion title",
            authorFirstName: "John",
            authorLastName: "Doe",
            createdAt: moment(),
            content: "fake content"}
        };
        wrapper = setUp(props);
        component = wrapper.find(".discussion-info")
    })
    

    it('should rend without errors', () => {
        expect(component.length).toBe(1)
    })

    it('should display title', () => {
        expect(wrapper.find(".card-title").text()).toEqual('Discussion title');
    })

    it('should have members count', () => {
        expect(component.containsMatchingElement(<p>fake content</p>)).toBe(true)
    })
});