import React from 'react'
import { shallow } from 'enzyme'
import SignedOutLinks from '../../../components/layout/SignedOutLinks';


describe("SignedOutLinks Component", () => {

    it('It should rend without errors', () => {
        const wrapper = shallow(<SignedOutLinks/>)
        const component = wrapper.find(".signed-out-links")
        expect(component.length).toBe(1)
    })
});