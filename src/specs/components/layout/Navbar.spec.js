import React from 'react'
import { shallow } from 'enzyme'
import { testStore } from '../../Utils/testStore';
import Navbar from '../../../components/layout/Navbar';
import firebaseMockData from '../../Utils/firebaseMockData'

const setUp = (initState) => {
    const store = testStore(initState)
    const wrapper = shallow(<Navbar store={store} />).dive()
    return wrapper
}

describe("Navbar Component", () => {

    let wrapper
    beforeEach(() => {
        const initState = {
                firebase: firebaseMockData
        }
        wrapper = setUp(initState)
    })

    it('should render without errors', () => {
        const component = wrapper.find(".nav-wrapper")
        expect(component.length).toBe(1)
    })
});