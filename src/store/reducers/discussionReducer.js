import {
    DISCUSSION_RESPONSE_FULFILLED,
    DISCUSSION_RESPONSE_PENDING,
    DISCUSSION_RESPONSE_RESET,
    DISCUSSION_RESPONSE_ERROR,
    DELETE_DISCUSSION,
    DELETE_DISCUSSION_ERROR
} from '../types/discussionTypes'

const initSate = {
    firebaseResponse: {
        error: null, 
        pending: false,
        fulfilled: false
    }
}

const discussionReducer = (state = initSate, action) => {
    switch (action.type) {
        case DISCUSSION_RESPONSE_PENDING:
            return {
                ...state,
                firebaseResponse: {error: null, pending: true, fulfilled: false}
            }
        case DISCUSSION_RESPONSE_FULFILLED:
            return {
                ...state,
                firebaseResponse: {error: null, pending: false, fulfilled: true}
            }
        case DISCUSSION_RESPONSE_ERROR:
            return {
                ...state,
                firebaseResponse: {error: action.err, pending: true, fulfilled: false}
            }
        case DISCUSSION_RESPONSE_RESET:
            return {
                ...state,
                firebaseResponse: {error: null, pending: false, fulfilled: false}
            }
        case DELETE_DISCUSSION:
            console.log('discussion deleted')
            return state;
        case DELETE_DISCUSSION_ERROR:
            console.log('discussion delete error', action.err)
            return state;
        default:
            return state;
    }
}

export default discussionReducer