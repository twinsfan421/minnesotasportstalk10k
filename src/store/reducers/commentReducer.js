import {
    COMMENT_CREATED,
    COMMENT_CREATED_ERROR,
    COMMENT_ACTION,
    COMMENT_ACTION_ERROR,
    COMMENT_ACTION_PENDING
} from '../types/commentTypes'

const initSate = {
    error: null,
    pending: false,
}

const commentReducer = (state = initSate, action) => {
    switch(action.type){
        case COMMENT_CREATED:
            return {
                ...state,
                error: null
            } 
        case COMMENT_CREATED_ERROR:
            return {
                ...state,
                error: action.err
            }
        case COMMENT_ACTION:
            return {
                ...state,
                pending: false
            }
        case COMMENT_ACTION_PENDING:
            return {
                ...state,
                pending: true
            }
        case COMMENT_ACTION_ERROR:
            return {
                ...state,
                pending: false
            }
        default:
            return state
    }
}

export default commentReducer