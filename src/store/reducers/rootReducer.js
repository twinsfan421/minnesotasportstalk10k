import authReducer from './authReducer';
import discussionReducer from './discussionReducer';
import { combineReducers } from 'redux';
import { firestoreReducer } from 'redux-firestore'
import { firebaseReducer } from 'react-redux-firebase'
import groupReducer from './groupReducer';
import commentReducer from './commentReducer';

const rootReducer = combineReducers({
    auth: authReducer,
    discussion: discussionReducer,
    group: groupReducer,
    comment: commentReducer,
    firestore: firestoreReducer,
    firebase: firebaseReducer
});

export default rootReducer