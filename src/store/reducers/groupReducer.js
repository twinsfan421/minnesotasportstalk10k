import {
    GROUP_RESPONSE_FULFILLED,
    GROUP_RESPONSE_PENDING,
    GROUP_RESPONSE_RESET,
    GROUP_RESPONSE_ERROR,
    DELETE_GROUP,
    DELETE_GROUP_ERROR,
    LEFT_GROUP_ERROR,
    JOIN_GROUP_ERROR,
    LEFT_GROUP,
    JOIN_GROUP
} from '../types/groupTypes'

const initSate = {
    firebaseResponse: {
        error: null, 
        pending: false,
        fulfilled: false
    }
}

const groupReducer = (state = initSate, action) => {
    switch (action.type) {
        case GROUP_RESPONSE_PENDING:
            return {
                ...state,
                firebaseResponse: {error: null, pending: true, fulfilled: false}
            }
        case GROUP_RESPONSE_FULFILLED:
            return {
                ...state,
                firebaseResponse: {error: null, pending: false, fulfilled: true}
            }
        case GROUP_RESPONSE_ERROR:
            return {
                ...state,
                firebaseResponse: {error: action.err, pending: true, fulfilled: false}
            }
        case GROUP_RESPONSE_RESET:
            return {
                ...state,
                firebaseResponse: {error: null, pending: false, fulfilled: false}
            }
        case DELETE_GROUP:
            console.log('group deleted')
            return state;
        case DELETE_GROUP_ERROR:
            console.log('group delete error', action.err)
            return state;
        case JOIN_GROUP_ERROR:
            console.log('joined group error', action.err)
            return state;
        case JOIN_GROUP:
            console.log('joined group')
            return state;
        case LEFT_GROUP:
            console.log('left group')
            return state;
        case LEFT_GROUP_ERROR:
            console.log('left group error', action.err)
            return state;
        default:
            return state;
    }
}

export default groupReducer