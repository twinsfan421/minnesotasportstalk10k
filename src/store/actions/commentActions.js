import {
    COMMENT_CREATED,
    COMMENT_CREATED_ERROR,
    COMMENT_ACTION,
    COMMENT_ACTION_ERROR,
    COMMENT_ACTION_PENDING
} from '../types/commentTypes'

export const createComment = (text, parentId, collection) => {
    return (dispatch, getState, { getFirebase, getFirestore}) => {
        const firestore = getFirestore();
        const profile = getState().firebase.profile;
        const userId = getState().firebase.auth.uid
        const name = profile.firstName + " " + profile.lastName
        firestore.collection(collection).doc(parentId).collection('comments').add({
            text: text, 
            commenterName: name,
            commenterId: userId,
            createdAt: new Date()
        }).then(() => {
            dispatch({type: COMMENT_CREATED});
        }).catch((err) => {
            dispatch({ type: COMMENT_CREATED_ERROR, err})
        })
    }
};

export const likeComment = (parentId, collection, id) => {
    return (dispatch, getState, { getFirebase, getFirestore}) => {
        const firestore = getFirestore();
        const userId = getState().firebase.auth.uid
        dispatch({type: COMMENT_ACTION_PENDING})
        firestore.collection(collection).doc(parentId).collection('comments').doc(id).update({
            likes: firestore.FieldValue.arrayUnion(userId)
        }).then(() => {
            dispatch({type: COMMENT_ACTION});
        }).catch((err) => {
            dispatch({ type: COMMENT_ACTION_ERROR, err})
        })
    }
};

export const unlikeComment = (parentId, collection, id) => {
    return (dispatch, getState, { getFirebase, getFirestore}) => {
        const firestore = getFirestore();
        const userId = getState().firebase.auth.uid
        dispatch({type: COMMENT_ACTION_PENDING})
        firestore.collection(collection).doc(parentId).collection('comments').doc(id).update({
            likes: firestore.FieldValue.arrayRemove(userId)
        }).then(() => {
            dispatch({type: COMMENT_ACTION});
        }).catch((err) => {
            dispatch({ type: COMMENT_ACTION_ERROR, err})
        })
    }
};