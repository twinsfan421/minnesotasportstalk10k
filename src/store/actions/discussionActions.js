import {
    DISCUSSION_RESPONSE_FULFILLED,
    DISCUSSION_RESPONSE_PENDING,
    DISCUSSION_RESPONSE_ERROR,
    DELETE_DISCUSSION,
    DELETE_DISCUSSION_ERROR
} from '../types/discussionTypes'

export const createDiscussion = (discussion, groupId) => {
    return (dispatch, getState, { getFirebase, getFirestore}) => {
        const firestore = getFirestore();
        const profile = getState().firebase.profile;
        const authorId = getState().firebase.auth.uid;
        dispatch({type: DISCUSSION_RESPONSE_PENDING})
        firestore.collection('discussions').add({
            ...discussion, 
            authorFirstName: profile.firstName,
            authorLastName: profile.lastName,
            authorId: authorId,
            groupId: groupId,
            createdAt: new Date()
        }).then(() => {
            dispatch({type: DISCUSSION_RESPONSE_FULFILLED, discussion});
        }).catch((err) => {
            dispatch({ type: DISCUSSION_RESPONSE_ERROR, err})
        })
    }
};

export const discussionEdit = (discussion) => {
    return (dispatch, getState, { getFirebase, getFirestore}) => {
        const firestore = getFirestore();
        dispatch({type: DISCUSSION_RESPONSE_PENDING})
        firestore.collection('discussions').doc(discussion.id).update({ 
            title: discussion.title,
            content: discussion.content,
            updatedAt: new Date()
        }).then(() => {
            dispatch({type: DISCUSSION_RESPONSE_FULFILLED, discussion});
        }).catch((err) => {
            dispatch({ type: DISCUSSION_RESPONSE_ERROR, err})
        })
    }
};

export const discussionDelete = (discussionId) => {
    return (dispatch, getState, { getFirebase, getFirestore}) => {
        const firestore = getFirestore();
        firestore.collection('discussions').doc(discussionId).delete().then(() => {
            dispatch({type: DELETE_DISCUSSION});
        }).catch((err) => {
            dispatch({ type: DELETE_DISCUSSION_ERROR, err})
        })
    }
};