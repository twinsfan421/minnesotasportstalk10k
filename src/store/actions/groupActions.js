import {
    GROUP_RESPONSE_FULFILLED,
    GROUP_RESPONSE_PENDING,
    GROUP_RESPONSE_ERROR,
    DELETE_GROUP,
    DELETE_GROUP_ERROR,
    LEFT_GROUP_ERROR,
    JOIN_GROUP_ERROR,
    LEFT_GROUP,
    JOIN_GROUP
} from '../types/groupTypes'

export const createGroup = (group) => {
    return (dispatch, getState, { getFirebase, getFirestore}) => {
        const firestore = getFirestore();
        const profile = getState().firebase.profile;
        const groupLeaderId = getState().firebase.auth.uid;
        dispatch({type: GROUP_RESPONSE_PENDING})
        firestore.collection('groups').add({
            ...group, 
            leaderFirstName: profile.firstName,
            leaderLastName: profile.lastName,
            leaderId: groupLeaderId,
            members: firestore.FieldValue.arrayUnion(groupLeaderId),
            createdAt: new Date()
        }).then(() => {
            dispatch({type: GROUP_RESPONSE_FULFILLED, group});
        }).catch((err) => {
            dispatch({ type:  GROUP_RESPONSE_ERROR, err})
        })
    }
};

export const groupEdit = (group) => {
    return (dispatch, getState, { getFirebase, getFirestore}) => {
        const firestore = getFirestore();
        dispatch({type: GROUP_RESPONSE_PENDING})
        firestore.collection('groups').doc(group.id).update({ 
            title: group.title,
            description: group.description,
            updatedAt: new Date()
        }).then(() => {
            dispatch({type:  GROUP_RESPONSE_FULFILLED, group});
        }).catch((err) => {
            dispatch({ type:  GROUP_RESPONSE_ERROR, err})
        })
    }
};

export const groupDelete = (groupId) => {
    return (dispatch, getState, { getFirebase, getFirestore}) => {
        const firestore = getFirestore();
        firestore.collection('groups').doc(groupId).delete().then(() => {
            dispatch({type: DELETE_GROUP});
        }).catch((err) => {
            dispatch({ type: DELETE_GROUP_ERROR, err})
        })
    }
};

export const joinGroup = (groupId) => {
    return (dispatch, getState, { getFirebase, getFirestore}) => {
        const firestore = getFirestore();
        const userId = getState().firebase.auth.uid
        firestore.collection('groups').doc(groupId).update({ 
            members: firestore.FieldValue.arrayUnion(userId)
        }).then(() => {
            dispatch({type: JOIN_GROUP});
        }).catch((err) => {
            dispatch({ type: JOIN_GROUP_ERROR, err})
        })
    }
};
export const leaveGroup = (groupId) => {
    return (dispatch, getState, { getFirebase, getFirestore}) => {
        const firestore = getFirestore();
        const userId = getState().firebase.auth.uid
        firestore.collection('groups').doc(groupId).update({ 
            members: firestore.FieldValue.arrayRemove(userId)
        }).then(() => {
            dispatch({type: LEFT_GROUP});
        }).catch((err) => {
            dispatch({ type: LEFT_GROUP_ERROR, err})
        })
    }
};