import React from 'react';
import MemberDetails from './MemberDetail'
import { connect } from 'react-redux'
import { firestoreConnect, isEmpty, isLoaded } from 'react-redux-firebase'
import { compose }  from 'redux'
import LinearPreloader from '../preloader/LinearPreloader';
import ResourceNotFound from '../error/ResourceNotFound';

const MemberList = ({ group }) => {
    if (!isLoaded(group)) {
        return <LinearPreloader />
    }
    if (isEmpty(group)) {
      return <ResourceNotFound />
    }
    var n = 0
    return (
        <div className="container">
            <div className="card blue-grey darken-1">
                <div className="card-content">
                    <h2 className="white-text center-align">{group.title + "  Member List"}</h2>
                    <h4 className="white-text center-align">
                        {"Group Creator: " + group.leaderFirstName + " " + group.leaderLastName}
                    </h4>
                    { group.members ? group.members.map(member => {
                        n += 1
                        return(
                             <MemberDetails member={member} key={member} n={n} />
                        )  
                    }) : null}
                </div>
            </div>
        </div>
    )
};

const mapStateToProps = (state, props) => {
    const id = props.match.params.id
    const groups = state.firestore.data.groups
    return {
        group: groups && groups[id]
    }
}

export default compose(
connect(mapStateToProps),
firestoreConnect(props =>[
    { collection: 'groups',
      doc: props.match.params.id
    }
])
)(MemberList);