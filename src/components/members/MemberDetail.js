import React from 'react';
import { connect } from 'react-redux'
import { firestoreConnect, isLoaded, isEmpty } from 'react-redux-firebase'
import { compose } from 'redux'
import LinearPreloader from '../preloader/LinearPreloader';

const MemberDetails = ({n, user}) => {
    if (!isLoaded(user)) {
        return <LinearPreloader />
    }
    if (isEmpty(user)) {
      return null
    }
    return(
        <div className="container" key={n}>
            <div className="card z-depth-0">
                <div className="card-content">
                    <span>{n}. {user.firstName} {user.lastName}</span>
                </div>
            </div>
        </div>
    )
};

const mapStateToProps = (state, props) => {
    const users = state.firestore.data.users
    return {
        user: users && users[props.member]
    }
}

export default compose(
    connect(mapStateToProps),
    firestoreConnect([
        { collection: 'users'}
    ])
)(MemberDetails);