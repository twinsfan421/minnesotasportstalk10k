import React from 'react'
import { Switch, Route } from 'react-router-dom'
import PrivateRoute from './PrivateRoute';
import Dashboard from '../dashboard/Dashboard';
import GroupsIndex from '../groups/GroupsIndex';
import DiscussionShow from '../discussions/DiscussionShow'
import SignIn from '../auth/SignIn';
import SignUp from '../auth/SignUp';
import GroupDiscussionsNew from '../groups/GroupDiscussionsNew';
import DiscussionDelete from '../discussions/DiscussionDelete';
import DiscussionEdit from '../discussions/DiscussionEdit';
import GroupNew from '../groups/GroupNew';
import GroupShow from '../groups/GroupShow'
import GroupEdit from '../groups/GroupEdit';
import GroupDelete from '../groups/GroupDelete';
import MemberList from '../members/MemberList';
import GroupDiscussions from '../groups/GroupDiscussions';
import DiscussionsIndex from '../discussions/DiscussionsIndex';
import NotFound from '../error/NotFound';

const Routes = () => {
    return (
        <Switch>
            <PrivateRoute exact path='/' component={Dashboard} />
            <Route path='/signin' component={SignIn}/>
            <Route path='/signup' component={SignUp}/>
            <PrivateRoute exact path='/discussions/:id' component={DiscussionShow} />
            <PrivateRoute exact path='/discussions/:id/delete' component={DiscussionDelete} />
            <PrivateRoute exact path='/discussions/:id/edit' component={DiscussionEdit} />
            <PrivateRoute exact path='/discussions' component={DiscussionsIndex} />
            <PrivateRoute exact path='/groups/:id/discussions/new' component={GroupDiscussionsNew}/>
            <PrivateRoute exact path='/groups/:id/discussions' component={GroupDiscussions} />
            <PrivateRoute exact path='/groups/:id' component={GroupShow} />
            <PrivateRoute exact path='/groups/:id/edit' component={GroupEdit} />
            <PrivateRoute exact path='/groups/:id/delete' component={GroupDelete} />
            <PrivateRoute exact path='/groups/:id/members' component={MemberList} />
            <PrivateRoute exact path='/groups' component={GroupsIndex} />
            <PrivateRoute exact path='/group/new' component={GroupNew}/>
            <Route path="" component={NotFound} />
        </Switch>
    )
};

export default Routes;