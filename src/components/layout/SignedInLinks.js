import React from 'react';
import { NavLink, Link } from "react-router-dom"
import { connect } from 'react-redux'
import { signOut} from '../../store/actions/authActions'
import { compose } from 'redux'
import { firebaseConnect } from 'react-redux-firebase'

const SingedInLinks = ({profile, signOut, firebase}) => {
    const handleClick = () => {
        firebase.logout()
        signOut()
    }
    return (  
        <ul className="right signed-in-links">
            <li className="hide-on-med-and-down"><NavLink to="/" exact>Home</NavLink></li>
            <li><NavLink to="/groups" exact>Groups</NavLink></li>
            <li><NavLink to="/group/new" exact>New Group</NavLink></li>
            <li><NavLink to="/discussions" exact>Discussions</NavLink></li>
            <li onClick={handleClick}><Link to="/">Log Out</Link></li>
            <li className="hide-on-med-and-down"><NavLink to="/" className="btn btn-floating blue lighten-1" exact>{profile && profile.initials}</NavLink></li>
        </ul>
    );
}


const mapStateToProps = (state) => {
    return {
        profile: state.firebase.profile
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        signOut: () => dispatch(signOut())
    }
}
export default compose(
    connect(mapStateToProps, mapDispatchToProps),
    firebaseConnect()
)(SingedInLinks);