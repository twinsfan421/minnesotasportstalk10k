import React from 'react';
import { Route } from "react-router-dom"
import GroupSubNav from './subnav/GroupSubNav';
import DiscussionSubNav from './subnav/DiscussionSubNav';

const SubNavbar = (props) => {
    return (
        <>
            <Route strict path="/groups/:id" component={GroupSubNav} />
            <Route strict path="/discussions/:id" component={DiscussionSubNav} />
        </>
    );
}
 
export default SubNavbar;