import React from 'react';
import { Link } from "react-router-dom"
import SignedInLinks from './SignedInLinks';
import SignedOutLinks from './SignedOutLinks';
import { connect } from 'react-redux'
import SubNavbar from './SubNavBar';

const Navbar = (props) => {
    const { auth } = props
    return (
        <nav className="nav-extended">
            <nav className="nav-wrapper grey darken-3">
                <div className="nav-container">
                    <Link to="/" className="brand-logo left hide-on-med-and-down">Minnesota Sports Talk</Link>
                    <Link to="/" className="brand-logo left hide-on-large-only">MST</Link>
                    {auth.isEmpty ? <SignedOutLinks /> : <SignedInLinks />}
                </div>
                <SubNavbar />
            </nav>
        </nav>
    );
}

const mapStateToProps = (state) => {
    return{
        auth: state.firebase.auth
    }
}
 
export default connect(mapStateToProps)(Navbar);
 