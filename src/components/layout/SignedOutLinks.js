import React from 'react';
import { NavLink } from "react-router-dom"


const SingedOutLinks = () => {
    return (  
        <ul className="signed-out-links right">
            <li><NavLink to="/signup" exact>Signup</NavLink></li>
            <li><NavLink to="/signin" exact>Login</NavLink></li>
        </ul>
    );
}
 
export default SingedOutLinks;