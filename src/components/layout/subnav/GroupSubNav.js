import React from 'react';
import { NavLink} from "react-router-dom"
import { connect } from 'react-redux'
import { compose } from 'redux'
import { firestoreConnect } from 'react-redux-firebase'

const GroupSubNav = (props) => {
    const { auth, group, match } = props
    return (
        <nav className="group-sub-navbar tabs tabs-transparent">
            <div className="nav-container">
                <ul>
                    <li><NavLink to={`/groups/${match.params.id}`} exact>Group</NavLink></li>
                    <li><NavLink to={`/groups/${match.params.id}/discussions/new`} exact>New Discussion</NavLink></li>
                    <li><NavLink to={`/groups/${match.params.id}/discussions`} exact>Discussions</NavLink></li>
                    <li><NavLink to={`/groups/${match.params.id}/members`} exact>Members</NavLink></li>
                    {group && group.leaderId === auth.uid ?
                            <>
                                <li><NavLink to={`/groups/${match.params.id}/edit`} exact>Edit</NavLink></li>
                                <li><NavLink to={`/groups/${match.params.id}/delete`} exact>Delete</NavLink></li>
                            </>
                            : null }
                </ul>
            </div>
        </nav>

    );
}

const mapStateToProps = (state, ownProps) => {
    const id = ownProps.match.params.id
    const groups = state.firestore.data.groups;
    const group = groups ? groups[id] : null  
    return {
        group: group,
        auth: state.firebase.auth
    }
}

export default compose(
    connect(mapStateToProps),
    firestoreConnect(props => [
        { collection: 'groups',
          doc: props.match.params.id,
        },
    ])
)(GroupSubNav);