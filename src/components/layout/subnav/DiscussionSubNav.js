import React from 'react';
import { NavLink } from "react-router-dom"
import { connect } from 'react-redux'
import { compose } from 'redux'
import { firestoreConnect, isLoaded, isEmpty } from 'react-redux-firebase'

const DiscussionSubNav = ({ auth, discussions, match}) => {
    if (!isLoaded(discussions)) {
        return null
    }
    const id = match.params.id
    const discussion = discussions[id]
    if (isEmpty(discussion)) {
        return null
    }
    return (
        <nav className="discussion-sub-navbar tabs tabs-transparent">
            <div className="nav-container">
                <ul>
                    <li><NavLink to={`/groups/${discussion.groupId}`} exact>Group</NavLink></li>
                    <li><NavLink to={`/discussions/${id}`} exact>Discussion</NavLink></li>
                    {discussion.authorId === auth.uid ?
                        <>
                            <li><NavLink to={`/discussions/${id}/edit`} exact>Edit</NavLink></li>
                            <li><NavLink to={`/discussions/${id}/delete`} exact>Delete</NavLink></li>
                        </>
                        : null }
                </ul>
            </div>
        </nav>
    )
}

const mapStateToProps = (state) => {
    return {
        auth: state.firebase.auth,
        discussions: state.firestore.data.discussions
    }
}

export default compose(
    connect(mapStateToProps),
    firestoreConnect(props => [
        { collection: 'discussions',
          doc: props.match.params.id
        },
    ])
)(DiscussionSubNav);