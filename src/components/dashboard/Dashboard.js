import React from 'react';
import Notifications from './Notifications'
import GroupList from '../groups/GroupList'
import { connect } from 'react-redux'
import { firestoreConnect, isLoaded } from 'react-redux-firebase'
import { compose } from 'redux'
import LinearPreloader from '../preloader/LinearPreloader';

const Dashboard = ({ groups, notifications }) => {
    
    if (!isLoaded(groups, notifications)) {
      return <LinearPreloader />
    }    

    return (
        <div className="dashboard container">
            <div className="row">
                <div className="col s12 m6">
                    <div className="card blue-grey darken-1 min-height">
                        <div className="card-content white-text">
                        <span className="card-title center-align">My Groups</span>
                            <GroupList groups={groups} />
                        </div>
                    </div>
                </div>
                <div className="col s12 m5 offset-m1">
                    <Notifications notifications={notifications} />
                </div>
            </div>   
        </div>
    );

}

const mapStateToProps = (state) => {
    return {
        groups: state.firestore.ordered.groups,
        auth: state.firebase.auth,
        notifications: state.firestore.ordered.notifications
    }
}

export default compose(
    connect(mapStateToProps),
    firestoreConnect(props => [
        { collection: 'groups' , limit: 100, orderBy: ['createdAt', 'desc'], where: ['members', 'array-contains', props.auth.uid]},
        { collection: 'notifications', limit: 10, orderBy: ['time', 'desc']}
    ])
)(Dashboard)