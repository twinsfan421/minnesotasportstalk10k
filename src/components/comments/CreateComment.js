import React, { Component } from 'react'
import { connect } from 'react-redux'
import { createComment } from '../../store/actions/commentActions'

class CreateComment extends Component {
    state = {
        text: ''
    }
    handleChange = (e) => {
        this.setState({ text: e.target.value })
    }
    handleSubmit = (e) => {
        e.preventDefault();
        this.props.createComment(this.state.text, this.props.parentId, this.props.collection)
        this.setState({text: ""})
    }

    render() {
        return (
            <div className="container create-comment">
               <form onSubmit={this.handleSubmit} className="white">
                   <h5 className="grey-text text-darken-3 center-align">Comment</h5>
                   <div className="input-field">
                       <input type="text" value={this.state.text} id="text" onChange={this.handleChange}/>
                   </div>
               </form>
            </div>
        )
    }
}


const mapDispatchToProps = (dispatch) => {
    return {
        createComment: (text, parentId, collection) => dispatch(createComment(text, parentId, collection))
    }
}

export default connect(null, mapDispatchToProps)(CreateComment)
