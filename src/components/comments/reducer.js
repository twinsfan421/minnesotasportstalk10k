import {useReducer} from 'react';

const initialState = {
    comments: [],
    loading: false,
    moreComments: true
};

const reducer = (state, action) => {
  switch (action.type) {
    case 'add':
      return {
          ...state,
          comments: [...state.comments, action.newComment],
          loading: false
        };
    case 'modify':
      return {
          ...state,
          comments: state.comments.map(comment =>
              comment.id === action.newComment.id ? action.newComment : comment
          ),
          loading: false
        };
    case 'noMoreComments':
      return {
          ...state,
          moreComments: false,
          loading: false
        };
    case 'loading':
      return {
        ...state,
        loading: true
      }
    default:
      throw new Error()
  }
};

export const getReducer = () => {
  // eslint-disable-next-line react-hooks/rules-of-hooks
  return useReducer(reducer, initialState);
};
  