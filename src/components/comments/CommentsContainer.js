import React from 'react';
import { firestoreConnect } from 'react-redux-firebase'
import PropTypes from 'prop-types'
import CreateComment from './CreateComment';
import {getReducer} from './reducer';
import LinearPreloader from '../preloader/LinearPreloader';
import Comment from './Comment';

const CommentsContainer = ({collection, parentId, firestore}) => {
    const [state, dispatch] = getReducer();
    const ref = firestore.collection(collection).doc(parentId).collection('comments').orderBy('createdAt')
    const comments = state.comments
    const limit = 10

    const handleSnapshot = (snapshot) => {
        let changes = snapshot.docChanges();
        if (changes.length) {
            changes.forEach(function(change) {
                if(change.type === 'added'){
                    dispatch({type: 'add', newComment: {id: change.doc.id, doc: change.doc, ...change.doc.data()}})
                }else if(change.type === 'modified'){
                    dispatch({type: 'modify', newComment: {id: change.doc.id, doc: change.doc, ...change.doc.data()}})
                }
            });
            if (changes.length < limit) dispatch({type: 'noMoreComments'})
        } else {
            dispatch({type: 'noMoreComments'})
        }
    }

    const getComments =  () => {
        dispatch({type: 'loading'})
        let last = comments[comments.length-1]
        let query = last ? ref.startAfter(last.doc).limit(limit) : ref.limit(limit)
        query.onSnapshot((snapshot) => handleSnapshot(snapshot));
        return
    }

    return (
        <div className="comments section">
            <ul className="collection with-header">
                <li className="collection-header center-align"><h6>Comments</h6></li>
                { comments && comments.map(comment => {
                    return(
                        <Comment collection={collection} parentId={parentId} comment={comment} key={comment.id}/>
                    )   
                })}
                { state.loading ? <LinearPreloader /> : null}
            </ul>
            { state.moreComments ? <button onClick={() => getComments()} className="btn-large blue lighten-2 full-width">Load Comments</button> : null }
            <CreateComment parentId={parentId} collection={collection} />
        </div>
    )
};

CommentsContainer.propTypes = {
    parentId: PropTypes.string,
    collection: PropTypes.string
}

export default firestoreConnect()(CommentsContainer);