import React from 'react';
import { connect } from 'react-redux'
import moment from 'moment'
import { likeComment, unlikeComment } from '../../store/actions/commentActions'

const Comment = ({auth, parentId, collection, comment, likeComment, unlikeComment}) => {
    const hasLiked = comment.likes && comment.likes.includes(auth.uid) ? true : false
    const likeIcon = hasLiked ? <i className="material-icons red-text" onClick={() => unlikeComment(parentId, collection, comment.id)}>favorite</i> :
    <i className="material-icons red-text" onClick={() => likeComment(parentId, collection, comment.id)}>favorite_border</i> 
    return (
        <li className="collection-item avatar">
            <i className="material-icons circle">face</i>
            <div className="secondary-content">
                { likeIcon }
                <span className="likes-count red-text">{comment.likes ? comment.likes.length : "0"}</span>
            </div>
            <p>{comment.text}</p>
            <span className="blue-text">{comment.commenterName}</span> <span className="grey-text text-darken-1">{moment(comment.createdAt.toDate()).calendar() } </span>
        </li>
    );
}

const mapStateToProps = (state) => {
    return {
        auth: state.firebase.auth
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        likeComment: (parentId, collection, id) => dispatch(likeComment(parentId, collection, id)),
        unlikeComment: (parentId, collection, id) => dispatch(unlikeComment(parentId, collection, id))
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Comment)
