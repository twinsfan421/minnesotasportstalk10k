import React from 'react'

const ResourceNotFound = () =>
    <div className="container group-not-found section">
        <div className="card">
            <div className="card-content center red">
                <h1 className="white-text">404</h1>
                <h2 className="white-text">Resource Not Found</h2>
                <p className="white-text">The Resource you are looking for does not exist.</p>
            </div>
        </div>
    </div>
export default ResourceNotFound