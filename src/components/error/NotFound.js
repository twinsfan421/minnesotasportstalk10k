import React from 'react'

const NotFound = () =>
    <div className="container not-found section">
        <div className="card">
            <div className="card-content center red">
                <h1 className="white-text">404</h1>
                <h2 className="white-text">Page Not Found</h2>
                <p className="white-text">The page you are looking for does not exist.</p>
            </div>
        </div>
    </div>
export default NotFound