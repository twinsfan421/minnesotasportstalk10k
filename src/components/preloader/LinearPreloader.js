import React from 'react'

const LinearPreloader = () => {
    return (
        <div className="progress">
            <div className="indeterminate"></div>
        </div>
    )
};

export default LinearPreloader;