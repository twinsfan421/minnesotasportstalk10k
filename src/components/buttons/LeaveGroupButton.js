import React from 'react';
import { connect } from 'react-redux'
import { leaveGroup } from '../../store/actions/groupActions'

const LeaveGroupButton = (props) => {
    return (  
        <button onClick={() => props.leaveGroup(props.id)} className="btn blue lighten-2">Leave Group</button>
    );
}

const mapDispatchToProps = (dispatch) => {
    return {
        leaveGroup: (groupId) => dispatch(leaveGroup(groupId))
    }
}

export default connect(null, mapDispatchToProps)(LeaveGroupButton)