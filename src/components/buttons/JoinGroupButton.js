import React from 'react';
import { connect } from 'react-redux'
import { joinGroup } from '../../store/actions/groupActions'

const JoinGroupButton = (props) => {
    return (  
        <button onClick={() => props.joinGroup(props.id)} className="btn blue lighten-2">Join Group</button>
    );
}

const mapDispatchToProps = (dispatch) => {
    return {
        joinGroup: (groupId) => dispatch(joinGroup(groupId))
    }
}

export default connect(null, mapDispatchToProps)(JoinGroupButton)