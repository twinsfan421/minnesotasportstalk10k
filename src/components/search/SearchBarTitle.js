import React, { useState } from 'react'
import SearchBarForm from '../forms/SearchBarForm';

const SearchBarTitle = ({ getFirestore }) => {
    const [value, setValue] = useState('');
    const [search, setSearch] = useState(false);
    const handleChange = (e) => {
        let value = e.target.value
        setValue(value)
        if (!value) value = false
        getFirestore(value)
    }
    const handleSubmit = (e) => {
        e.preventDefault();
        setSearch(value)
        setValue('')
    }
    const handleCancleSearch = (e) => {
        setSearch(false)
        getFirestore(false)
    }

    return (
        <div className="search-bar card grey lighten-2">
            <SearchBarForm value={value} handleChange={handleChange} handleSubmit={handleSubmit} />
            { search ? <><div className="chip blue lighten-2">{search}</div>
                <button className="btn-floating btn-small  red" onClick={handleCancleSearch}><i className="material-icons">close</i></button></> : null }
        </div>
    )
}

export default SearchBarTitle