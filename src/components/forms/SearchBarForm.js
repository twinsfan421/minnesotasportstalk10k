import React from 'react';

const SearchBarForm = ({value, handleChange, handleSubmit}) => {
    return(
        <form onSubmit={handleSubmit}>
            <label className="input-field black-text"htmlFor="term">Search By Title</label>
            <input type="text" id="term" value={value} onChange={handleChange} required/>
        </form>
    )
};

export default SearchBarForm;