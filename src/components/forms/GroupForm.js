import React from 'react';
import CirclePreloader from '../preloader/CirclePreloader'


const GroupForm = ({firebaseResponse, group, handleChange, handleSubmit}) => {
    return(
        <form onSubmit={handleSubmit} className="white group-form">
            <label htmlFor="title">Group Title</label>
            <div className="input-field">
                <input type="text" id="title" defaultValue={group.title} onChange={handleChange} required/>
            </div>
            <label htmlFor="content">Group Description</label>
            <div className="input-field">
                <textarea className="materialize-textarea" defaultValue={group.description} id="description"  onChange={handleChange} required/>
            </div>
            <div className="input-field">
                { firebaseResponse.pending ? <CirclePreloader /> : <button className="btn-large blue lighten-2 z-depth-0">Submit<i className="material-icons right">send</i></button>}
                { firebaseResponse.error ? <p className="red-text center">{firebaseResponse.error}</p> : null}
            </div>
        </form>
    )
};

export default GroupForm;