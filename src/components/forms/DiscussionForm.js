import React from 'react';
import CirclePreloader from '../preloader/CirclePreloader'


const DiscussionForm = ({firebaseResponse, discussion, handleChange, handleSubmit}) => {
    return(
        <form onSubmit={handleSubmit} className="white discussion-form">
            <label htmlFor="title">Title</label>
            <div className="input-field">
                <input type="text" id="title" defaultValue={discussion.title} onChange={handleChange} required/>
            </div>
            <label htmlFor="content">Discussion Content</label>
            <div className="input-field">
                <textarea className="materialize-textarea" defaultValue={discussion.content} id="content"  onChange={handleChange} required/>
            </div>
            <div className="input-field">
                { firebaseResponse.pending ? <CirclePreloader /> : <button className="btn-large blue lighten-2 z-depth-0">Submit<i className="material-icons right">send</i></button>}
                { firebaseResponse.error ? <p className="red-text center">{firebaseResponse.error}</p> : null}
            </div>
        </form>
    )
};

export default DiscussionForm;