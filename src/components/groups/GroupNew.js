import React, { Component } from 'react'
import { connect } from 'react-redux'
import { createGroup } from '../../store/actions/groupActions'
import { GROUP_RESPONSE_RESET } from '../../store/types/groupTypes'
import GroupForm from '../forms/GroupForm'

class GroupNew extends Component {
    state = {
        title: '',
        description: ''
    }
    handleChange = (e) => {
        this.setState({
            [e.target.id]: e.target.value
        })
    }
    handleSubmit = (e) => {
        e.preventDefault();
        this.props.createGroup(this.state)
    }
    componentDidUpdate() {
        if (this.props.firebaseResponse.fulfilled) {
            const M = window.M;
            M.toast({html: `Create Group ${this.state.title} Successful!`})
            this.props.history.push('/');
        }
    }
    componentWillUnmount() {
        this.props.groupResponseReset()
    }
    render() {
        const { firebaseResponse } = this.props;

        return (
            <div className="container create-group">
                <div className="card">
                    <h3 className="grey-text text-darken-3 center section">New Group</h3>
                    <GroupForm firebaseResponse={firebaseResponse} group={this.state} handleSubmit={this.handleSubmit} handleChange={this.handleChange} />
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        firebaseResponse: state.group.firebaseResponse
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        createGroup: (group) => dispatch(createGroup(group)),
        groupResponseReset: () => dispatch({type: GROUP_RESPONSE_RESET })
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(GroupNew)
