import React from 'react';
import DiscussionSummary from '../discussions/DiscussionSummary'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { firestoreConnect, isEmpty, isLoaded } from 'react-redux-firebase'
import { compose } from 'redux'
import LinearPreloader from '../preloader/LinearPreloader';
import ResourceNotFound from '../error/ResourceNotFound';

const GroupDiscussions = ({ discussions, group }) => {
    if (!isLoaded(group, discussions)) {
      return <LinearPreloader />
    }
    if (isEmpty(group)) {
      return <ResourceNotFound />
    }
    return (
        <div className="group-discussions container">
            <div className="col s12 m9">
                <div className="card blue-grey darken-1">
                    <div className="card-content white-text">
                        <h3 className="center-align">{group.title}</h3>
                        <h4 className="center-align">Discussions</h4>   
                        { discussions && discussions.map(discussion => {
                            return(
                                <Link to={'/discussions/' + discussion.id} key={discussion.id} >
                                    <DiscussionSummary discussion={discussion} />
                                </Link>
                            )   
                        })}
                    </div>
                </div>
            </div>
        </div>
    )
};

const mapStateToProps = (state, props) => {
    const id = props.match.params.id
    const groups = state.firestore.data.groups
    const discussions = state.firestore.ordered.discussions     
    return {
        group: groups && groups[id],
        discussions: discussions
    }
}

export default (compose(
    connect(mapStateToProps),
    firestoreConnect(props => [
        { collection: 'groups',
          doc: props.match.params.id,
        },
        { collection: 'discussions' ,
          where: ['groupId', '==', props.match.params.id],
          orderBy: ['createdAt', 'desc'] 
        },
    ])
)(GroupDiscussions));