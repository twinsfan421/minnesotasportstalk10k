import React from 'react';
import { connect } from 'react-redux'
import { firestoreConnect, isLoaded, isEmpty } from 'react-redux-firebase'
import { compose } from 'redux'
import CommentsContainer from '../comments/CommentsContainer'
import Notifications from '../dashboard/Notifications'
import JoinGroupButton from '../buttons/JoinGroupButton';
import LeaveGroupButton from '../buttons/LeaveGroupButton';
import LinearPreloader from '../preloader/LinearPreloader';
import GroupInfo from './GroupInfo';
import ResourceNotFound from '../error/ResourceNotFound';

const GroupShow = ({ group, auth, comments, notifications, match}) => {
    if (!isLoaded(group, notifications)) {
        return <LinearPreloader />
    }
    if (isEmpty(group)) {
      return <ResourceNotFound />
    }
    const id = match.params.id
    const inGroup = group.members && group.members.includes(auth.uid) ? true : false
    return (
        <div className="container section group-show">
            <div className="row">
                <div className="col s12 l8">
                    <div className="card blue-grey darken-1 min-height">
                        <div className="card-content white-text">
                            <h3 className="center-align">{ group.title }</h3>
                            <h5 className="center-align">{ group.description }</h5>
                            { inGroup ? <LeaveGroupButton id={id} /> : <JoinGroupButton id={id} /> }
                        </div>
                        <div className="card-action blue-grey darken-2">
                            <GroupInfo group={group} />
                            <CommentsContainer parentId={id} collection={"groups"} comments={comments}/>
                        </div>
                    </div>
                </div>
                <div className="col m12 l4">
                    <Notifications notifications={notifications} />
                </div>
            </div>
        </div>
    )
}

const mapStateToProps = (state, props) => {
    const id = props.match.params.id
    const groups = state.firestore.data.groups
    return {
        group: groups && groups[id],
        auth: state.firebase.auth,
        comments: state.firestore.ordered.comments,
        notifications: state.firestore.ordered.groupnotifications
    }
}

export default (compose(
    connect(mapStateToProps),
    firestoreConnect(props => [
        { collection: 'groups',
          doc: props.match.params.id,
        },

        { collection: 'groupnotifications',
          where: ['groupId', '==', props.match.params.id],
          limit: 5, orderBy: ['time', 'desc']
        }
    ])
)(GroupShow));