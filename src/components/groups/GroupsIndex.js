import React, { useState, useEffect } from 'react';
import { firestoreConnect } from 'react-redux-firebase'
import LinearPreloader from '../preloader/LinearPreloader';
import GroupSummary from './GroupSummary'
import { Link } from 'react-router-dom'
import InfiniteScroll from 'react-infinite-scroller';
import SearchBarTitle from '../search/SearchBarTitle';

const GroupsIndex = ({ firestore }) => {
    const [groups, setGroups] = useState();
    const [query, setQuery] = useState();
    const [moreGroups, setMoreGroups] = useState(false);
    const ref = firestore.collection('groups')
    const getGroups = async (term = false) => {
        setGroups(null)
        let groupsQuery = term === false ? ref.orderBy('createdAt', 'desc') : ref.where('titleSearch', 'array-contains', term.toLowerCase()).orderBy('createdAt', 'desc')
        setQuery(groupsQuery)
        let snapshot = await groupsQuery.limit(20).get()
        let newGroups = snapshot.docs
        if (newGroups.length === 0) return setGroups([])
        setGroups(newGroups)
        return checkGroupsLength(newGroups.length)
    }

    const getMoreGroups = async () => {
        const last = groups[groups.length-1]
        let snapshot = await query.startAfter(last).limit(20).get()
        let newGroups = snapshot.docs
        checkGroupsLength(newGroups.length)
        return setGroups(groups => groups.concat(newGroups))
    }
    
    const checkGroupsLength = (length) => {
        length < 20 ? setMoreGroups(false) : setMoreGroups(true)
    }

    useEffect(() => {
        getGroups();
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);
    const groupLinks = []
    groups && groups.map((group) => {
        return(
            groupLinks.push(
                <Link to={'/groups/' + group.id} key={group.id}>
                <GroupSummary group={group.data()} />
                </Link>
            )
        )
    })
    return (
        <div className="dashboard container all-groups">
            <SearchBarTitle  getFirestore={getGroups} />
            {!groups ? <LinearPreloader /> : null }
            <InfiniteScroll
                pageStart={0}
                loadMore={getMoreGroups}
                hasMore={moreGroups}
                loader={<div className="loader" key={0}><LinearPreloader /></div>}
                >
                <div className="section">
                    {groupLinks}
                </div>
            </InfiniteScroll>
        </div>
    );
}

export default firestoreConnect()(GroupsIndex)