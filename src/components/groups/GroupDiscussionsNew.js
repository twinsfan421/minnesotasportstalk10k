import React, { Component } from 'react'
import { connect } from 'react-redux'
import { createDiscussion } from '../../store/actions/discussionActions'
import { DISCUSSION_RESPONSE_RESET } from '../../store/types/discussionTypes'
import DiscussionForm from '../forms/DiscussionForm'

class GroupDiscussionsNew extends Component {
    state = {
        title: '',
        content: ''
    }
    handleChange = (e) => {
        this.setState({
            [e.target.id]: e.target.value
        })
    }
    handleSubmit = (e) => {
        e.preventDefault();
        this.props.createDiscussion(this.state, this.props.match.params.id)
    }
    componentDidUpdate() {
        if (this.props.firebaseResponse && this.props.firebaseResponse.fulfilled) {
            const M = window.M;
            M.toast({html: `Create Discussion Successful: ${this.state.title}`})
            this.props.history.push('/groups/' + this.props.match.params.id + '/discussions');
        }
    }
    componentWillUnmount() {
        this.props.discussionResponseReset()
    }
    render() {
        const { firebaseResponse } = this.props;
        return (
            <div className="container create-discussion">
                <div className="card">
                    <h3 className="grey-text text-darken-3 center section">New Discussion</h3>
                    <DiscussionForm firebaseResponse={firebaseResponse} discussion={this.state} handleSubmit={this.handleSubmit} handleChange={this.handleChange} />
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        firebaseResponse: state.discussion.firebaseResponse,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        createDiscussion: (discussion, groupId) => dispatch(createDiscussion(discussion, groupId)),
        discussionResponseReset: () => dispatch({type: DISCUSSION_RESPONSE_RESET })
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(GroupDiscussionsNew)
