import React, { Component } from 'react'
import { connect } from 'react-redux'
import { groupEdit } from '../../store/actions/groupActions'
import { Redirect } from 'react-router-dom'
import { firestoreConnect, isLoaded, isEmpty } from 'react-redux-firebase'
import { compose } from 'redux'
import LinearPreloader from '../preloader/LinearPreloader'
import { GROUP_RESPONSE_RESET } from '../../store/types/groupTypes'
import ResourceNotFound from '../error/ResourceNotFound'
import GroupForm from '../forms/GroupForm'

class GroupEdit extends Component {
    state = {
        title: this.props.group ? this.props.group.title : null,
        description:this.props.group ? this.props.group.description : null
    }
    handleChange = (e) => {
        this.setState({
            [e.target.id]: e.target.value
        })
    }
    handleSubmit = (e) => {
        e.preventDefault()
        const group = {
            id: this.props.match.params.id,
            title: this.state.title,
            description: this.state.description
        }
        this.props.groupEdit(group)
    }
    componentDidUpdate() {
        if (this.props.firebaseResponse.fulfilled) {
            const M = window.M;
            M.toast({html: `Edit ${this.props.group.title} Successful!`})
            this.props.history.push('/groups/' + this.props.match.params.id)
        }
        if (this.state.title === null && this.props.group) {
            this.setState({
                title: this.props.group.title,
                description: this.props.group.description
            })
        }
    }
    componentWillUnmount() {
        this.props.groupResponseReset()
    }
    render() {
        const { auth, group, firebaseResponse } = this.props;
        if (!isLoaded(group)) {
            return <LinearPreloader />
        }
        if (isEmpty(group)) {
          return <ResourceNotFound />
        }
        if (group.leaderId !== auth.uid) return <Redirect to='/' />
        return (
            <div className="container group-edit">
                <div className="card">
                    <h3 className="grey-text text-darken-3 center section">Edit Group</h3>
                    <GroupForm firebaseResponse={firebaseResponse} group={group} handleSubmit={this.handleSubmit} handleChange={this.handleChange} />
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state, props) => {
    const id = props.match.params.id
    const groups = state.firestore.data.groups
    return {
        auth: state.firebase.auth,
        group: groups && groups[id],
        firebaseResponse: state.group.firebaseResponse
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        groupEdit: (group) => dispatch(groupEdit(group)),
        groupResponseReset: () => dispatch({type: GROUP_RESPONSE_RESET })
    }
}

export default compose(
    connect(mapStateToProps, mapDispatchToProps),
    firestoreConnect(props => [
        { collection: 'groups',
          doc: props.match.params.id
        }
    ])
)(GroupEdit);