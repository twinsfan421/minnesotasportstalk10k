import React from 'react';
import GroupSummary from './GroupSummary'
import { Link } from 'react-router-dom'

const GroupList = ({groups}) => {
    return (
        <div className="group-list section">  
            { groups && groups.map(group => {
                return(
                    <Link to={'/groups/' + group.id} key={group.id} >
                      <GroupSummary group={group} />
                    </Link>
                )   
            })}
        </div>
    )
};

export default GroupList;