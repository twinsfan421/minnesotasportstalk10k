import React from 'react'
import moment from 'moment'

const GroupInfo = (props) => {
    const group = props.group
    const members = group.members
    return (
        <div className="group-info">
            <span className="white-text">Group Creator: {group.leaderFirstName} {group.leaderLastName}</span>
            <p className="white-text">Group created at: {moment(group.createdAt.toDate()).calendar() }</p>
            {group.updatedAt ? <p className="white-text">
                Group updated at: {moment(group.updatedAt.toDate()).calendar() }</p> : null }
            <p className="white-text">{"Members Count: " + members.length }</p>
        </div>
    )
};

export default GroupInfo;