import React from 'react'
import { connect } from 'react-redux'
import { groupDelete } from '../../store/actions/groupActions'
import { Redirect } from 'react-router-dom'
import { firestoreConnect, isLoaded, isEmpty } from 'react-redux-firebase'
import { compose } from 'redux'
import LinearPreloader from '../preloader/LinearPreloader'
import ResourceNotFound from '../error/ResourceNotFound'

const GroupDelete = ({groupDelete, match, group, history, auth}) => {
    const deleteSubmit = (e) => {
        groupDelete(match.params.id)
        history.push('/');
    }
    if (!isLoaded(group)) {
      return <LinearPreloader />
    }
    if (isEmpty(group)) {
      return <ResourceNotFound />
    }
    if (group.leaderId !== auth.uid) return <Redirect to='/' />
    return (
        <div className="container section group-delete">
            <div className="card z-depth-0">
                <div className="card-content">
                    <h3>Are you sure you want to delete this Group?</h3>
                    <span className="card-title">{ group.title }</span>
                    <p>{ group.description }</p>
                </div>
                <div className="card-action grey lighten-4 grey-text">
                    <button onClick={deleteSubmit} className="btn-large blue lighten-2 z-depth-0">Delete Group</button>
                </div>
            </div>
        </div>
    )
}

const mapStateToProps = (state, props) => {
    const id = props.match.params.id
    const groups = state.firestore.data.groups
    return {
        auth: state.firebase.auth,
        group: groups && groups[id]
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        groupDelete: (group) => dispatch(groupDelete(group))
    }
}

export default compose(
    connect(mapStateToProps, mapDispatchToProps),
    firestoreConnect(props => [
        { collection: 'groups',
          doc: props.match.params.id
        }
    ])
)(GroupDelete);