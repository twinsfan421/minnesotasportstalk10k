import React from 'react';
import moment from 'moment'

const GroupSummary = ({group}) => {
    return(
        <div className="card z-depth-0 group-summary">
            <div className="card-content grey-text text-darken-3">
                <span className="card-title">{group.title}</span>
                <p>{group.description}</p>
                <p>Group Leader: {group.leaderFirstName} {group.leaderLastName}</p>
                <p className="grey-text">{moment(group.createdAt.toDate()).calendar() }</p>
            </div>
        </div>
    )
};

export default GroupSummary;