import React, { useState, useEffect } from 'react';
import { firestoreConnect } from 'react-redux-firebase'
import LinearPreloader from '../preloader/LinearPreloader';
import DiscussionSummary from './DiscussionSummary'
import { Link } from 'react-router-dom'
import InfiniteScroll from 'react-infinite-scroller';
import SearchBarTitle from '../search/SearchBarTitle';

const DiscussionsIndex = ({ firestore }) => {
    const [discussions, setDiscussions] = useState();
    const [query, setQuery] = useState();
    const [moreDiscussions, setMoreDiscussions] = useState(false);
    const ref = firestore.collection('discussions')
    const getDiscussions = async (term = false) => {
        setDiscussions(null)
        let discussionsQuery = term === false ? ref.orderBy('createdAt', 'desc') : ref.where('titleSearch', 'array-contains', term.toLowerCase()).orderBy('createdAt', 'desc')
        setQuery(discussionsQuery)
        let snapshot = await discussionsQuery.limit(20).get()
        let newDiscussions = snapshot.docs
        if (newDiscussions.length === 0) return setDiscussions([])
        setDiscussions(newDiscussions)
        return checkDiscussionsLength(newDiscussions.length)
    }

    const getMoreDiscussions = async () => {
        const last = discussions[discussions.length-1]
        let snapshot = await query.startAfter(last).limit(20).get()
        let newDiscussions = snapshot.docs
        checkDiscussionsLength(newDiscussions.length)
        return setDiscussions(discussions => discussions.concat(newDiscussions))
    }
    
    const checkDiscussionsLength = (length) => {
        length < 20 ? setMoreDiscussions(false) : setMoreDiscussions(true)
    }

    useEffect(() => {
        getDiscussions();
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const discussionLinks = []
    discussions && discussions.map((discussion) => {
        return(
            discussionLinks.push(
                <Link to={'/discussions/' + discussion.id} key={discussion.id}>
                    <DiscussionSummary discussion={discussion.data()} />
                </Link>
            )
        )
    })
    return (
        <div className="dashboard container discussions-index">
            <SearchBarTitle  getFirestore={getDiscussions} />
            {!discussions ? <LinearPreloader /> : null }
            <InfiniteScroll
                pageStart={0}
                loadMore={getMoreDiscussions}
                hasMore={moreDiscussions}
                loader={<div className="loader" key={0}><LinearPreloader /></div>}
                >
                <div className="section">
                    {discussionLinks}
                </div>
            </InfiniteScroll>
        </div>
    );
}


export default firestoreConnect()(DiscussionsIndex)
