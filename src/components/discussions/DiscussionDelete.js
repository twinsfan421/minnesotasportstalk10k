import React from 'react'
import { connect } from 'react-redux'
import { discussionDelete } from '../../store/actions/discussionActions'
import { Redirect } from 'react-router-dom'
import { firestoreConnect, isLoaded, isEmpty } from 'react-redux-firebase'
import { compose } from 'redux'
import LinearPreloader from '../preloader/LinearPreloader'
import ResourceNotFound from '../error/ResourceNotFound';

const DiscussionDelete  = ({discussionDelete, match, discussion, history, auth}) => {
    const deleteSubmit = (e) => {
        discussionDelete(match.params.id)
        history.push('/');
    }
    if (!isLoaded(discussion)) {
        return <LinearPreloader />
    }
    if (isEmpty(discussion)) {
        return <ResourceNotFound />
    }
    if (discussion.authorId !== auth.uid) return <Redirect to='/' />
    return (
        <div className="container section discussion-delete">
            <div className="card z-depth-0 min-height">
                <div className="card-content">
                    <h3>Are you sure you want to delete this Discussion?</h3>
                    <span className="card-title">{ discussion.title }</span>
                    <p>{ discussion.content }</p>
                </div>
                <div className="card-action grey lighten-4 grey-text">
                    <button onClick={deleteSubmit} className="btn-large blue lighten-2 z-depth-0">Delete Discussion</button>
                </div>
            </div>
        </div>
    )
}

const mapStateToProps = (state, props) => {
    const id = props.match.params.id;
    const discussions = state.firestore.data.discussions;
    return {
        auth: state.firebase.auth,
        discussion: discussions && discussions[id]
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        discussionDelete: (discussion) => dispatch(discussionDelete(discussion))
    }
}

export default compose(
    connect(mapStateToProps, mapDispatchToProps),
    firestoreConnect([
        { collection: 'discussions' }
    ])
)(DiscussionDelete);