import React from 'react'
import moment from 'moment'

const DiscussionInfo = (props) => {
    const discussion = props.discussion
    return (
        <div className="card-content discussion-info">
            <span className="card-title">{ discussion.title }</span>
            <p>{ discussion.content }</p>
            <div className="section">
                <p className="grey-text text-darken-2">Posted by: {discussion.authorFirstName} {discussion.authorLastName}</p>
                <p className="grey-text text-darken-2">Created at: {moment(discussion.createdAt.toDate()).calendar() }</p>
                { discussion.updatedAt ? <div className="red-text">
                    Updated at:{moment(discussion.updatedAt.toDate()).calendar() }</div> : null }
            </div>
        </div>
    )
};

export default DiscussionInfo;