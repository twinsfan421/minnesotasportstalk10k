import React, { Component } from 'react'
import { connect } from 'react-redux'
import { discussionEdit } from '../../store/actions/discussionActions'
import { Redirect } from 'react-router-dom'
import { firestoreConnect, isLoaded, isEmpty } from 'react-redux-firebase'
import { compose } from 'redux'
import LinearPreloader from '../preloader/LinearPreloader'
import { DISCUSSION_RESPONSE_RESET } from '../../store/types/discussionTypes'
import ResourceNotFound from '../error/ResourceNotFound';
import DiscussionForm from '../forms/DiscussionForm'

class DiscussionEdit extends Component {
    state = {
        title: this.props.discussion ? this.props.discussion.title : null,
        content: this.props.discussion ? this.props.discussion.content : null
    }
    handleChange = (e) => {
        this.setState({
            [e.target.id]: e.target.value
        })
    }
    handleSubmit = (e) => {
        e.preventDefault()
        const discussion = {
            id: this.props.match.params.id,
            title: this.state.title,
            content: this.state.content
        }
        this.props.discussionEdit(discussion)
    }
    componentDidUpdate() {
        if (this.props.firebaseResponse.fulfilled) {
            const M = window.M;
            M.toast({html: `Edit Successful: ${this.props.discussion.title}`})
            this.props.history.push('/discussions/' + this.props.match.params.id)
        }
        if (this.state.title === null && this.props.discussion) {
            this.setState({
                title: this.props.discussion.title,
                content: this.props.discussion.content
            })
        }
    }
    componentWillUnmount() {
        this.props.discussionResponseReset()
    }

    render() {
        const { auth, discussion, firebaseResponse} = this.props;
        if (!isLoaded(discussion)) {
            return <LinearPreloader />
        }
        if (isEmpty(discussion)) {
          return <ResourceNotFound />
        }
        if (discussion.authorId !== auth.uid) return <Redirect to='/' />
        return (
            <div className="container discussion-edit">
                <div className="card">
                    <h3 className="grey-text text-darken-3 center section">Edit Discussion</h3>
                    <DiscussionForm firebaseResponse={firebaseResponse} discussion={discussion} handleSubmit={this.handleSubmit} handleChange={this.handleChange} />
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state, props) => {
    const id = props.match.params.id;
    const discussions = state.firestore.data.discussions;
    return {
        auth: state.firebase.auth,
        discussion: discussions && discussions[id],
        firebaseResponse: state.discussion.firebaseResponse,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        discussionEdit: (discussion) => dispatch(discussionEdit(discussion)),
        discussionResponseReset: () => dispatch({type: DISCUSSION_RESPONSE_RESET })
    }
}

export default compose(
    connect(mapStateToProps, mapDispatchToProps),
    firestoreConnect([
        { collection: 'discussions' }
    ])
)(DiscussionEdit);