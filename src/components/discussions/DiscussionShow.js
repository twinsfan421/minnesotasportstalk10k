import React from 'react';
import { connect } from 'react-redux'
import { firestoreConnect, isLoaded, isEmpty } from 'react-redux-firebase'
import { compose } from 'redux'
import CommentsContainer from '../comments/CommentsContainer'
import LinearPreloader from '../preloader/LinearPreloader'
import DiscussionInfo from './DiscussionInfo'
import ResourceNotFound from '../error/ResourceNotFound';

const DiscussionShow = ({ discussion, comments, match }) => {
    if (!isLoaded(discussion)) {
        return <LinearPreloader />
    }
    if (isEmpty(discussion)) {
      return <ResourceNotFound />
    }
    const id = match.params.id
    return (
        <div className="container section discussion-details">
            <div className="card z-depth-0 min-height">
                <DiscussionInfo discussion={discussion}  />
                <div className="card-action grey lighten-4 grey-text text-darken-3">
                    <CommentsContainer comments={comments} parentId={id} collection={"discussions"} />
                </div>
            </div>
        </div>
    )
}

const mapStateToProps = (state, props) => {
    const id = props.match.params.id;
    const discussions = state.firestore.data.discussions;
    return {
        discussion: discussions && discussions[id],
        comments: state.firestore.ordered.comments
    }
}

export default compose(
    connect(mapStateToProps),
    firestoreConnect( props => [
        { collection: 'discussions',
          doc: props.match.params.id
        },
        { collection: 'discussions',
          doc: props.match.params.id,
          subcollections: [
              {
                collection: 'comments',
                limit: 5,   
                orderBy: ['createdAt', 'desc']
              }
          ],
            storeAs: "comments"
        }
    ])
)(DiscussionShow);