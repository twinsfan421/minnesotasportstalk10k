import React from 'react';
import moment from 'moment'

const DiscussionSummary = ({discussion}) => {
    return(
        <div className="card z-depth-0 discussion-summary">
            <div className="card-content grey-text text-darken-3">
                <span className="card-title">{discussion.title}</span>
                <p>{discussion.content}</p>
                <p>Posted By {discussion.authorFirstName} {discussion.authorLastName}</p>
                <p className="grey-text">{moment(discussion.createdAt.toDate()).calendar() }</p>
            </div>
        </div>
    )
};

export default DiscussionSummary;