const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp(functions.config().firebase);

const createSearchArray = (title) => {
    let lower = title.toLowerCase()
    let searchArray = []
    let curName = ''
    lower.split('').forEach(l => {
        curName += l
        searchArray.push(curName)
    })
    return searchArray
}

const createNotification = (notification => { 
    return admin.firestore().collection('notifications')
    .add(notification)
    .then(doc => console.log('notification added', doc));
})

const createGroupNotification = (notification => { 
    return admin.firestore().collection('groupnotifications')
    .add(notification)
    .then(doc => console.log('group notification added', doc));
})

exports.discussionCreated = functions.firestore
    .document('discussions/{discussionId}')
    .onCreate(doc => {

        const discussion = doc.data();
        const notification = {
            content: `Added a new discussion: ${discussion.title}`,
            user: `${discussion.authorFirstName} ${discussion.authorLastName}`,
            groupId: discussion.groupId,
            time: admin.firestore.FieldValue.serverTimestamp()
        }

        return createGroupNotification(notification);
    
})

exports.discussionEdited = functions.firestore
    .document('discussions/{discussionId}')
    .onUpdate(doc => {

        const discussion = doc.after.data();
        const notification = {
            content: `Edited discussion: ${discussion.title}`,
            user: `${discussion.authorFirstName} ${discussion.authorLastName}`,
            groupId: discussion.groupId,
            time: admin.firestore.FieldValue.serverTimestamp()
        }

        return createGroupNotification(notification);
    
})

exports.discussionDeleted = functions.firestore
    .document('discussions/{discussionId}')
    .onDelete(doc => {

        const discussion = doc.previous.data();
        const notification = {
            content: `Deleted discussion: ${discussion.title}`,
            user: `${discussion.authorFirstName} ${discussion.authorLastName}`,
            groupId: discussion.groupId,
            time: admin.firestore.FieldValue.serverTimestamp()
        }

        return createGroupNotification(notification);
    
})

exports.groupCreated = functions.firestore
    .document('groups/{groupId}')
    .onCreate(doc => {

        const group = doc.data();
        const notification = {
            content: `Added a new Group: ${group.title}`,
            user: `${group.leaderFirstName} ${group.leaderLastName}`,
            time: admin.firestore.FieldValue.serverTimestamp()
        }

        return createNotification(notification);
    
})

exports.titleSearchGroups = functions.firestore
    .document('groups/{groupId}')
    .onWrite(doc => {

        if (!doc.after.exists) return null

        const data = doc.after.data();
        const previousData = doc.before.data();

        if (data.title === previousData.title) return null;

        let searchArray = createSearchArray(data.title)

        return doc.after.ref.set({
            titleSearch: searchArray
          }, {merge: true});
});

exports.titleSearchDiscussions = functions.firestore
    .document('discussions/{discussionId}')
    .onWrite(doc => {

        if (!doc.after.exists) return null

        const data = doc.after.data();
        const previousData = doc.before.data();

        if (data.title === previousData.title) return null;

        let searchArray = createSearchArray(data.title)

        return doc.after.ref.set({
            titleSearch: searchArray
          }, {merge: true});
});

exports.groupEdited = functions.firestore
    .document('groups/{groupId}')
    .onUpdate(doc => {

        const group = doc.after.data();
        const notification = {
            content: `Edited group: ${group.title}`,
            user: `${group.leaderFirstName} ${group.leaderLastName}`,
            time: admin.firestore.FieldValue.serverTimestamp()
        }

        return createNotification(notification);
    
})

exports.groupDeleted = functions.firestore
    .document('groups/{groupId}')
    .onDelete(doc => {

        const group = doc.previous.data();
        const notification = {
            content: `Deleted group: ${group.title}`,
            user: `${group.leaderFirstName} ${group.leaderLastName}`,
            time: admin.firestore.FieldValue.serverTimestamp()
        }

        return createNotification(notification);
    
})

exports.userJoined = functions.firestore
    .document('users/{uid}')
    .onCreate(doc => {
        const user = doc.data();
        const notification = {
            content: 'New User Joined!',
            user: `${user.firstName} ${user.lastName}`,
            time: admin.firestore.FieldValue.serverTimestamp()
        }

        return createNotification(notification);
})